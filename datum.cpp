#include "datum.h"

bool Datum::from_string(const string &f) {
    istringstream ss(f);
    ss >> date::parse("%Y-%m-%d", d);
    return ss.fail();
}

string Datum::to_string() const {
    ostringstream ss;
    ss << date::format("%Y-%m-%d", d);
    return ss.str();
}

const date::year_month_day& Datum::get_date() const {
    return d;
}

int Datum::diff(const Datum& other) const
{
    auto t = date::sys_days{d}-date::sys_days{other.get_date()};
    return t.count();
}

bool Datum::between(const Datum &start, const Datum &end) const
{
    return start.get_date() < d && end.get_date() > d;
}

ostream& operator<<(ostream& os, const Datum& dt)
{
    os << date::format("%Y-%b-%d", dt.get_date());
    return os;
}

void from_json(const json &j, Datum &d) {
    d.from_string(j);
}

void to_json(json &j, const Datum &d)
{
    j = d.to_string();
}
