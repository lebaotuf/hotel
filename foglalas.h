#ifndef FOGLALAS_H
#define FOGLALAS_H

#include <vector>
#include <map>
#include <iostream>

#include "vendeg.h"
#include "arajanlat.h"
#include "datum.h"
#include "felhasznalo.h"
#include "szobatipus.h"
#include "szamla.h"

using namespace std;

class Foglalas
{
    string lefoglalo;
    vector<Vendeg> vendegek;
    Datum kezdete;
    Datum vege;
    int allapot;
    vector<int> szobaszamok;
    int ellatas;
    double szamolt_ar;
    Szamla szamla;
public:
    Foglalas();
    Foglalas(const Arajanlat& arajanlat, const vector<Vendeg>& v,
             const string& email, const vector<int>& szobaszamok);
    bool bejelentkezes();
    bool kijelentkezes();
    bool modositas();
    bool torles();
    const string& leker_ellatas();
    void megjelenit() const;
    int szoba_kereses(int szobaszam) const;
    void tetel_felszamolas(const string& nev, int darab, double ar);

    void setSzamolt_ar(double newSzamolt_ar);
    const string &getLefoglalo() const;
    int getAllapot() const;
    int getEllatas() const;
    int getVendegek_szama() const;
    int getSzobaszam() const;
    const Datum &getKezdete() const;
    const Datum &getVege() const;
    const Szamla &getSzamla() const;

    friend void from_json(const json &je, Foglalas &f);
    friend void to_json(json &je, const Foglalas& f);
    friend ostream& operator<<(ostream& os, const Foglalas& dt);
    void setEllatas(int newEllatas);
};

void from_json(const json &je, Foglalas& f);
void to_json(json &je, const Foglalas& f);
ostream& operator<<(ostream& os, const Foglalas& dt);

#endif // FOGLALAS_H
