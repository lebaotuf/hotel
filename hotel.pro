TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle
CONFIG -= qt
INCLUDEPATH = ../external/

SOURCES += \
    arajanlat.cpp \
    datum.cpp \
    felhasznalo.cpp \
    foglalas.cpp \
    hotel.cpp \
    main.cpp \
    szamla.cpp \
    szobatipus.cpp \
    util.cpp \
    vendeg.cpp

HEADERS += \
    arajanlat.h \
    datum.h \
    felhasznalo.h \
    foglalas.h \
    hotel.h \
    loadexception.h \
    szamla.h \
    szobatipus.h \
    util.h \
    vendeg.h
