#include "foglalas.h"

void Foglalas::setSzamolt_ar(double newSzamolt_ar)
{
    szamolt_ar = newSzamolt_ar;
}

const string &Foglalas::getLefoglalo() const
{
    return lefoglalo;
}

int Foglalas::getAllapot() const
{
    return allapot;
}

int Foglalas::getEllatas() const
{
    return ellatas;
}

int Foglalas::getVendegek_szama() const
{
    return vendegek.size();
}

int Foglalas::getSzobaszam() const
{
    return szobaszamok[0];
}

const Datum &Foglalas::getKezdete() const
{
    return kezdete;
}

const Datum &Foglalas::getVege() const
{
    return vege;
}

const Szamla &Foglalas::getSzamla() const
{
    return szamla;
}

void Foglalas::setEllatas(int newEllatas)
{
    ellatas = newEllatas;
}

Foglalas::Foglalas()
{

}

Foglalas::Foglalas(const Arajanlat& arajanlat, const vector<Vendeg>& v,
                   const string &email, const vector<int>& szobaszamok):
    lefoglalo(email),
    vendegek(v),
    szobaszamok(szobaszamok)
{
    kezdete = arajanlat.getKezdes();
    vege = arajanlat.getVegzes();
    szamolt_ar = arajanlat.getSzamolt_ar();
    ellatas = arajanlat.getEllatas();
    allapot = h_enum::foglalt;
}

bool Foglalas::bejelentkezes()
{
    allapot = h_enum::bejelentkezett;
    return true;
}

bool Foglalas::kijelentkezes()
{
    allapot = h_enum::kijelentkezett;
    szamla.tetel_hozzadasa("Szállásdíj", 1, szamolt_ar);
    return true;
}

int Foglalas::szoba_kereses(int szobaszam) const
{
    auto it = find(szobaszamok.begin(), szobaszamok.end(), szobaszam);
    if(it == szobaszamok.end()) {
        return -1;
    } else {
        return *it;
    }
}

void Foglalas::tetel_felszamolas(const string &nev, int darab, double ar)
{
    szamla.tetel_hozzadasa(nev, darab, ar);
}

ostream& operator<<(ostream& os, const Foglalas& dt)
{
    map<int,string> ellatas_megnevezesek{
        {h_enum::nincs, "Nincs"},
        {h_enum::reggeli, "Reggeli"},
        {h_enum::felpanzio, "Félpanzió"},
        {h_enum::teljes, "Teljes"}
    };
    os << "Ellátás: " << ellatas_megnevezesek[dt.ellatas] << endl;
    os << "Ár: " << dt.szamolt_ar << endl;
    os << "Kezdete: " << dt.kezdete << endl;
    os << "Vége: " << dt.vege << endl;
    return os;
}

void from_json(const json &je, Foglalas &f)
{
    f.lefoglalo = je["azonosito"];
    transform(je["vendegek"].begin(),
             je["vendegek"].end(),
             back_inserter(f.vendegek),
             [](const json& vendeg)
    {
        Vendeg v(vendeg["nev"].get<string>(),
                 vendeg["szuldat"].get<Datum>());
        return v;
    });
    f.kezdete = je["kezdete"].get<Datum>();
    f.vege = je["vege"].get<Datum>();
    f.allapot = je["allapot"].get<int>();
    f.szobaszamok = je["szobak"].get<vector<int>>();
    f.ellatas = je["ellatas"].get<int>();
    f.szamolt_ar = je["szamolt_ar"].get<double>();
}

void to_json(json &je, const Foglalas &f)
{
    je = json{{"azonosito",f.lefoglalo},
              {"vendegek", f.vendegek},
              {"kezdete",f.kezdete},
              {"vege",f.vege},
              {"allapot",f.allapot},
              {"szobak", f.szobaszamok},
              {"ellatas", f.ellatas},
              {"szamolt_ar", f.szamolt_ar}};
}
