#ifndef HOTEL_H
#define HOTEL_H

#include <nlohmann/json.hpp>
#include <fstream>
#include <list>
#include <vector>
#include <iostream>
#include <iomanip>
#include <memory>
#include <date.h>
#ifdef __linux__
// getpass (csak Linuxon)
#include <unistd.h>
#endif

#include "szobatipus.h"
#include "arajanlat.h"
#include "felhasznalo.h"
#include "foglalas.h"
#include "loadexception.h"
#include "util.h"

#define INPUT_FILE "../input.json"
#define OUTPUT_FILE "../output.json"

using namespace std;
using json = nlohmann::json;

class Hotel
{
    shared_ptr<Felhasznalo> jelenlegi_felhasznalo;
    list<Foglalas> foglalasok;
    vector<RegisztraltVendeg> regisztralt_vendegek;
    vector<Dolgozo> dolgozok;
    vector<Szobatipus> szobatipusok;
    void betolt();
    void ment();
public:
    Hotel();
    int ellatottak_lekerese(Datum nap, int etkezes);
    Arajanlat arajanlat_keszites() const;
    void foglalas();
    void regisztracio();
    bool belepes();
    void foglalasok_vendeg();
    void foglalasok_recepcios();
    void foglalas_torles();
    void szoba_lekeres();
    void ellatottak_szama();
    void vendeg_ellatas_lekerese();
    void bejelentkeztetes();
    void kijelentkeztetes();
    void fogyasztas_hozzaadas();
    void alkalmazott_torlese();
    void alkalmazott_hozzadasa();
    void szoba_modositas();
    void foglalas_modositas();
    void start();
};

#endif // HOTEL_H
