#include "felhasznalo.h"

vector<pair<int,string>> menu_nevek ={
    {m_regisztracio, "Regisztráció"},
    {m_arajanlat, "Árajanlat készítés"},
    {m_foglalas, "Foglalás"},
    {m_foglalas_lekeres_vendeg, "Foglalás adatainak lekérése"},
    {m_foglalas_lekeres_recepcios, "Foglalás adatainak lekérése"},
    {m_foglalas_torles, "Foglalás törlése"},
    {m_fogyasztas_hozzaadas, "Fogyasztás felszámolása"},
    {m_ellatas_lekeres, "Vendég ellátásának lekérése"},
    {m_ellatotak_szama, "Éttermi vendégek száma"},
    {m_foglalas_modositas, "Foglalás módosítása"},
    {m_szoba_allapot_lekeres, "Szoba állapot lekérése"},
    {m_bejelentkeztetes, "Bejelentkeztetés"},
    {m_kijelentkeztetes, "Kijelentkeztetés"},
    {m_alkalmazott_hozzaadas, "Alkalmazott hozzáadása"},
    {m_alkalmazott_torles, "Alkalazott törlése"},
    {m_szoba_modositas, "Szoba adatainak módosítása"},
    {m_kilepes, "Kilépés"}
};

const string &Dolgozo::getLogin() const
{
    return azonosito;
}

vector<int> Dolgozo::getMenulista() const
{
    switch (munkakor) {
    case h_enum::ettermi:
        return {m_fogyasztas_hozzaadas, m_ellatas_lekeres, m_ellatotak_szama, m_kilepes};
        break;
    case h_enum::recepcios:
        return {m_foglalas_modositas, m_szoba_allapot_lekeres, m_bejelentkeztetes,
                    m_kijelentkeztetes, m_arajanlat, m_foglalas, m_foglalas_lekeres_recepcios,
                    m_foglalas_torles, m_kilepes};
        break;
    case h_enum::admin:
        return {m_alkalmazott_hozzaadas, m_alkalmazott_torles, m_szoba_modositas, m_kilepes};
        break;
    default:
        return {};
    }
}

bool Dolgozo::azonositas(const pair<string, string> &input) const
{
    return (!input.first.compare(azonosito) && !input.second.compare(jelszo));
}

bool RegisztraltVendeg::azonositas(const pair<string, string> &input) const
{
    return (!input.first.compare(email) && !input.second.compare(jelszo));
}

bool Admin::azonositas(const pair<string, string> &input) const
{
    return (!input.first.compare(azonosito) && !input.second.compare(jelszo));
}

void from_json(const json &j, Dolgozo &d)
{
    d.azonosito = j["azonosito"].get<string>();
    d.munkakor = j["munkakor"];
    d.jelszo = j["jelszo"].get<string>();
}

void to_json(json &j, const Dolgozo &d)
{
    j = json{{"azonosito", d.azonosito},
             {"munkakor", d.munkakor},
             {"jelszo", d.jelszo}};
}

void from_json(const json &j, RegisztraltVendeg &rv)
{
    rv.nev = j["nev"].get<string>();
    rv.email = j["email"].get<string>();
    rv.jelszo = j["jelszo"].get<string>();
}

void to_json(json &j, const RegisztraltVendeg &rv)
{
    j = json{{"nev", rv.nev},
             {"email", rv.email},
             {"jelszo", rv.jelszo}};
}

Felhasznalo::Felhasznalo(const string &jelszo) : jelszo(jelszo)
{}

const string &RegisztraltVendeg::getLogin() const
{
    return email;
}

const string &RegisztraltVendeg::getNev() const
{
    return nev;
}

vector<int> RegisztraltVendeg::getMenulista() const
{
    return {m_arajanlat, m_foglalas, m_foglalas_lekeres_vendeg, m_kilepes/*, m_foglalas_torles*/};
}

RegisztraltVendeg::RegisztraltVendeg(const string &nev, const string &email, const string &jelszo) : Felhasznalo(jelszo),
    email(email),
    nev(nev)
{}

Dolgozo::Dolgozo()
{}

Dolgozo::Dolgozo(const string &azonosito, int munkakor, const string &jelszo) :
    Felhasznalo(jelszo),
    azonosito(azonosito),
    munkakor(munkakor)
{}
