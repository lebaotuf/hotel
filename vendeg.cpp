#include "vendeg.h"

Vendeg::Vendeg()
{

}

Vendeg::Vendeg(const string &nev, const Datum &szuldat) : nev(nev),
    szuldat(szuldat)
{}

void to_json(json &j, const Vendeg &v)
{
    j = json{{"nev",v.nev},
             {"szuldat", v.szuldat}};
}
