#include "szamla.h"

Szamla::Szamla()
{

}

void Szamla::tetel_hozzadasa(const string &megnevezes, int darab, double ar)
{
    Tetel t;
    t.nev = megnevezes;
    t.darab = darab;
    t.ar = ar;
    tetelek.push_back(t);
}

void Szamla::kiir() const
{
    cout << "Számla: " << endl;
    cout << "Megnevezés\t\tDarabszám\t\tÁr" << endl;
    for(auto &t: tetelek) {
        cout << t.nev << "\t" << t.darab << "\t\t" << t.ar << endl;
    }
}
