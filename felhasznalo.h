#ifndef FELHASZNALO_H
#define FELHASZNALO_H

#include <string>
#include <iostream>
#include <vector>

#include "datum.h"
#include "util.h"

using namespace std;

enum menupontok {m_regisztracio, m_arajanlat, m_foglalas, m_foglalas_lekeres_vendeg,
                 m_foglalas_lekeres_recepcios, m_foglalas_torles, m_fogyasztas_hozzaadas,
                 m_ellatas_lekeres, m_ellatotak_szama, m_foglalas_modositas,
                 m_szoba_allapot_lekeres, m_bejelentkeztetes, m_kijelentkeztetes,
                 m_alkalmazott_hozzaadas, m_alkalmazott_torles, m_szoba_modositas,
                 m_kilepes};

extern vector<pair<int,string>> menu_nevek;

class Felhasznalo {
protected:
    string jelszo;
public:
    Felhasznalo(){};
    Felhasznalo(const string &jelszo);
    virtual bool azonositas(const pair<string,string>& input) const =0;
    virtual const string &getLogin() const =0;
    virtual vector<int> getMenulista() const =0;
};

void to_json(json& j, const shared_ptr<Felhasznalo> &f);


/* ------------------------------------------ */

class Dolgozo: public Felhasznalo {
    string azonosito;
    int munkakor;
public:
    Dolgozo();
    Dolgozo(const string &azonosito, int munkakor, const string& jelszo);
    virtual ~Dolgozo() = default;
    bool azonositas(const pair<string,string>& input) const;
    friend void from_json(const json &j, Dolgozo &d);
    friend void to_json(json &j, const Dolgozo &d);
    const string &getLogin() const;
    vector<int> getMenulista() const;
};

void from_json(const json &j, Dolgozo &d);
void to_json(json &j, const Dolgozo &d);


/* ------------------------------------------ */

class RegisztraltVendeg: public Felhasznalo {
    string email;
    string nev;
public:
    virtual ~RegisztraltVendeg() = default;
    RegisztraltVendeg(){};
    RegisztraltVendeg(const string &nev, const string &email, const string &jelszo);
    bool azonositas(const pair<string,string>& input) const;
    friend void from_json(const json &j, RegisztraltVendeg &rv);
    friend void to_json(json &j, const RegisztraltVendeg &rv);
    const string &getLogin() const;
    const string &getNev() const;
    vector<int> getMenulista() const;
};

void from_json(const json &j, RegisztraltVendeg &rv);
void to_json(json &j, const RegisztraltVendeg &rv);


/* ------------------------------------------ */

class Admin: public Felhasznalo {
    string azonosito;
public:
    bool azonositas(const pair<string,string>& input) const;
};

#endif // FELHASZNALO_H
