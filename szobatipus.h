#ifndef SZOBATIPUS_H
#define SZOBATIPUS_H

#include <vector>
#include <nlohmann/json.hpp>

using namespace std;
using json = nlohmann::json;

class Szobatipus
{
    string megnevezes;
    int ferohely;
    double alapar;
    double ar_fejenkent;
    double ar_fejenkent_gyerek;
    vector<int> szobaszamok;
public:
    Szobatipus();
    Szobatipus(const string& megnevezes,
               int ferohely,
               double alapar,
               double ar_fejenkent,
               double ar_fejenkent_gyerek,
               const vector<int> &szobaszamok);
    const string &getMegnevezes() const;
    double getAlapar() const;
    double getAr_fejenkent() const;
    double getAr_fejenkent_gyerek() const;

    int szoba_kereses(int szobaszam) const;
    const vector<int>& szoba_lista() const;

    friend void from_json(const json &je, Szobatipus &sz);
    friend void to_json(json &je, const Szobatipus &sz);

    void setAlapar(double newAlapar);
};

void from_json(const json &je, Szobatipus &sz);
void to_json(json &je, const Szobatipus &sz);
#endif // SZOBATIPUS_H
