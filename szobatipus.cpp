#include "szobatipus.h"

double Szobatipus::getAlapar() const
{
    return alapar;
}

double Szobatipus::getAr_fejenkent() const
{
    return ar_fejenkent;
}

double Szobatipus::getAr_fejenkent_gyerek() const
{
    return ar_fejenkent_gyerek;
}

int Szobatipus::szoba_kereses(int szobaszam) const
{
    auto it = find(szobaszamok.begin(), szobaszamok.end(), szobaszam);
    if(it == szobaszamok.end()) {
        return -1;
    } else {
        return *it;
    }
}

const vector<int> &Szobatipus::szoba_lista() const
{
    return szobaszamok;
}

const string &Szobatipus::getMegnevezes() const
{
    return megnevezes;
}

void Szobatipus::setAlapar(double newAlapar)
{
    alapar = newAlapar;
}

Szobatipus::Szobatipus()
{

}

Szobatipus::Szobatipus(const string &megnevezes,
                       int ferohely,
                       double alapar,
                       double ar_fejenkent,
                       double ar_fejenkent_gyerek,
                       const vector<int> &szobaszamok) : megnevezes(megnevezes),
    ferohely(ferohely),
    alapar(alapar),
    ar_fejenkent(ar_fejenkent),
    ar_fejenkent_gyerek(ar_fejenkent_gyerek),
    szobaszamok(szobaszamok)
{}

void from_json(const json &je, Szobatipus &sz)
{
    sz.megnevezes = je["megnevezes"].get<string>();
    sz.ferohely = je["ferohely"];
    sz.alapar = je["alapar"];
    sz.ar_fejenkent = je["ar"];
    sz.ar_fejenkent_gyerek = je["ar_gyerek"];
    sz.szobaszamok = je["szobaszamok"].get<vector<int>>();
}

void to_json(json &je, const Szobatipus &sz)
{
    je = json{{"megnevezes", sz.megnevezes},
              {"ferohely", sz.ferohely},
              {"alapar", sz.alapar},
              {"ar", sz.ar_fejenkent},
              {"ar_gyerek", sz.ar_fejenkent_gyerek},
              {"szobaszamok", sz.szobaszamok}};
}
