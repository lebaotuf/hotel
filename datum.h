#ifndef DATUM_H
#define DATUM_H

#include <nlohmann/json.hpp>
#include <date.h>

using namespace std;
using json = nlohmann::json;

class Datum {
    date::year_month_day d;
public:
    bool from_string(const string& f);
    string to_string() const;
    const date::year_month_day& get_date() const;
    int diff(const Datum& other) const;
    bool between(const Datum& start, const Datum& end) const;
};

ostream& operator<<(ostream& os, const Datum& dt);
void from_json(const json &j, Datum &d);
void to_json(json& j, const Datum& d);

#endif // DATUM_H

