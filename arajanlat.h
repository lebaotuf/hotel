#ifndef ARAJANLAT_H
#define ARAJANLAT_H

#include "szobatipus.h"
#include "datum.h"
#include "util.h"

class Arajanlat
{
    static double AFA;
    Szobatipus szoba;
    int felnottek;
    int gyerekek;
    Datum kezdes;
    Datum vegzes;
    double szamolt_ar;
    int ellatas;
    string kupon;
public:
    Arajanlat() = default;
    Arajanlat(const Szobatipus &szoba, int felnottek, int gyerekek,
              const Datum &kezdes, const Datum &vegzes, int ellatas);
    double ajanlat();
    int getLetszam();
    const Datum &getKezdes() const;
    const Datum &getVegzes() const;
    double getSzamolt_ar() const;
    int getEllatas() const;
    const Szobatipus &getSzobatipus() const;
    void setKupon(const string &newKuponkod);
    double kedvezmenySzamitas();
};

#endif // ARAJANLAT_H
