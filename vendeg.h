#ifndef VENDEG_H
#define VENDEG_H

#include "datum.h"

using namespace std;

class Vendeg
{
    string nev;
    Datum szuldat;
public:
    Vendeg();
    Vendeg(const string &nev, const Datum &szuldat);
    friend void to_json(json& j, const Vendeg& v);
};

void to_json(json& j, const Vendeg& v);
#endif // VENDEG_H
