#ifndef UTIL_H
#define UTIL_H

#include <string>
#include <iostream>
#include <vector>

#include "datum.h"

namespace h_enum {
enum allapot_enum {foglalt, bejelentkezett, kijelentkezett};
enum ellatas_enum {nincs, reggeli, felpanzio, teljes};
enum munkakor_enum {recepcios, ettermi, admin};
}

using namespace std;
int beker_modositasra_int(const string& szoveg, int jelenlegi);
string beker_string_ures();
string beker_string(const string& szoveg);
int beker_int(const string &szoveg);
int beker_lista(const vector<string>& listaelemek);
Datum beker_datum(const string& szoveg);


#endif // UTIL_H
