#ifndef LOADEXCEPTION_H
#define LOADEXCEPTION_H

#include <exception>

class LoadException : public std::exception {
public:
    virtual const char *what() const throw(){
        return "Error during file loading";
    }
};

#endif // LOADEXCEPTION_H
