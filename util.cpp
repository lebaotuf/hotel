#include "util.h"

string beker_string_ures()
{
    string eredmeny;
    getline(cin, eredmeny);
    return eredmeny;
}

string beker_string(const string &szoveg) {
    string eredmeny;
    bool ujra = false;
    do {
        if(ujra) {
            cout << "Kérem ne hagyja üresen a mezőt!" << endl;
        }
        cout << szoveg;
        eredmeny = beker_string_ures();
        ujra = true;
    } while (eredmeny.empty());
    return eredmeny;
}

int beker_int(const string &szoveg) {
    int eredmeny;
    string eredmenystr;
    bool ures = false;
    bool hibas_szam = false;
    do {
        if(ures && !hibas_szam) {
            cout << "Kérem ne hagyja üresen a mezőt!" << endl;
        } else if (!ures && hibas_szam) {
            cout << "Kérem adjon meg egy számot!" << endl;
        }
        cout << szoveg;
        getline(cin, eredmenystr);
        if (!eredmenystr.empty()) {
            try {
                eredmeny = stoi(eredmenystr);
                hibas_szam = false;
            }  catch (const invalid_argument& ex) {
                ex.what();
                hibas_szam = true;
            }
            ures = false;
        } else {
            ures = true;
        }
    } while (ures || hibas_szam);
    return eredmeny;
}

int beker_lista(const vector<string> &listaelemek)
{
    int i=1;
    unsigned valasz;
    for(const auto &elem: listaelemek) {
        cout << "(" << i << ") " << elem << endl;
        i++;
    }
    do {
    valasz = beker_int("Kérem adjon meg egy számot (1-" + to_string(listaelemek.size()) + "): ");
    } while(valasz > listaelemek.size());
    return valasz;
}

Datum beker_datum(const string& szoveg) {
    Datum d;
    bool hibas_datum = false;
    do {
        if(hibas_datum) {
            cout << "A megadott dátum hibás" << endl;
        }
        hibas_datum = d.from_string(beker_string(szoveg));
    } while(hibas_datum);
    return d;
}

int beker_modositasra_int(const string &szoveg, int jelenlegi)
{
    cout << szoveg << jelenlegi << endl;
    cout << "Új érték: ";
    int eredmeny;
    bool hibas_szam = false;
    do {
        string input = beker_string_ures();
        if(input.empty()) {
            return jelenlegi;
        } else {
            try {
                eredmeny = stoi(input);
                hibas_szam = false;
            }  catch (const invalid_argument& ex) {
                ex.what();
                hibas_szam = true;
            }
        }
    } while(hibas_szam);
    return eredmeny;
}
