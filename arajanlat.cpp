#include "arajanlat.h"

double Arajanlat::AFA = 0.27;

double Arajanlat::kedvezmenySzamitas() {
    if(!kupon.compare("K1000")) {
        return 1000.0;
    } else if(!kupon.compare("K5SZAZALEK"))
    {
        return 0.05;
    } else {
        return 0.0;
    }
}

double Arajanlat::ajanlat()
{
    double netto = 0;
    netto = szoba.getAlapar() + vegzes.diff(kezdes)*(felnottek*szoba.getAr_fejenkent() + gyerekek*szoba.getAr_fejenkent_gyerek());
    if(!kupon.empty()) {
        double kedvezmeny = kedvezmenySzamitas();
        if (kedvezmeny < 1.0 && kedvezmeny > 0.0) {
            netto *= (1.0-kedvezmeny);
        } else {
            netto -= kedvezmeny;
        }
    }
    szamolt_ar = netto;
    return netto;
}

int Arajanlat::getLetszam()
{
    return felnottek+gyerekek;
}


const Datum &Arajanlat::getKezdes() const
{
    return kezdes;
}

const Datum &Arajanlat::getVegzes() const
{
    return vegzes;
}

double Arajanlat::getSzamolt_ar() const
{
    return szamolt_ar;
}

int Arajanlat::getEllatas() const
{
    return ellatas;
}

const Szobatipus &Arajanlat::getSzobatipus() const
{
    return szoba;
}

void Arajanlat::setKupon(const string &newKuponkod)
{
    kupon = newKuponkod;
}

Arajanlat::Arajanlat(const Szobatipus &szoba, int felnottek, int gyerekek,
                     const Datum &kezdes, const Datum &vegzes, int ellatas) :
    szoba(szoba),
    felnottek(felnottek),
    gyerekek(gyerekek),
    kezdes(kezdes),
    vegzes(vegzes),
    ellatas(ellatas)
{}
