#ifndef SZAMLA_H
#define SZAMLA_H

#include <vector>
#include <iostream>

#include "vendeg.h"

class Szamla
{
    struct Tetel {
        string nev;
        int darab;
        double ar;
    };

    Vendeg nev;
    vector<Tetel> tetelek;
public:
    Szamla();
    void tetel_hozzadasa(const string& megnevezes,
                          int darab,
                          double ar);
    void kiir() const;
};

#endif // SZAMLA_H
