#include <iostream>
#include <fstream>
#include <chrono>
#include <sstream>
#include <algorithm>
#include <vector>
#include <nlohmann/json.hpp>

#include "hotel.h"
#include "datum.h"

using namespace std;

int main()
{
    try {
        Hotel h;
        h.start();
    }  catch (LoadException& ex) {
        cout << ex.what() << endl;
    }

    return 0;
}
