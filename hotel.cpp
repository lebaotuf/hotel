#include "hotel.h"

void Hotel::betolt()
{
    ifstream i(INPUT_FILE);
    vector<Datum> datumLista;
    if(i.is_open()) {
        nlohmann::json j;
        i >> j;

        for_each(j["regisztralt_vendegek"].begin(),
                 j["regisztralt_vendegek"].end(),
                 [&](const json &je){
            regisztralt_vendegek.push_back(je.get<RegisztraltVendeg>());
        });
        for_each(j["dolgozok"].begin(),
                 j["dolgozok"].end(),
                 [&](const json &je){
            dolgozok.push_back(je.get<Dolgozo>());
        });
        for_each(j["foglalasok"].begin(),
                 j["foglalasok"].end(),
                 [&](const json &je){
            foglalasok.push_back(je.get<Foglalas>());
        });
        for_each(j["szobatipusok"].begin(),
                 j["szobatipusok"].end(),
                 [&](const json &je){
            szobatipusok.push_back(je.get<Szobatipus>());
        });
    } else {
        throw LoadException();
    }
}

void Hotel::ment()
{
    ofstream o(OUTPUT_FILE);
    json oj;
    oj["regisztralt_vendegek"] = json(regisztralt_vendegek);
    oj["dolgozok"] = json(dolgozok);
    oj["foglalasok"] = json(foglalasok);
    oj["szobatipusok"] = json(szobatipusok);
    o<<setw(4)<<oj;
    o.close();
}

Hotel::Hotel()
{
    betolt();
}

Arajanlat Hotel::arajanlat_keszites() const
{
    double ar;
    vector<string> listaelemek;
    transform(szobatipusok.begin(),
              szobatipusok.end(),
              back_inserter(listaelemek),
              [](const Szobatipus& szoba){
        return szoba.getMegnevezes();
    });
    int szoba_idx = beker_lista(listaelemek)-1;
    int felnottek = beker_int("Felnőttek: ");
    int gyerekek = beker_int("Gyerekek: ");
    Datum kezdes, vegzes;
    do {
        kezdes = beker_datum("Kezdő dátum: ");
        vegzes = beker_datum("Vége dátum: ");
    } while((vegzes.diff(kezdes)) < 1);
    int ellatas = beker_lista({"Nincs", "Reggeli", "Félpanzió", "Teljes"})-1;
    string kuponkod;
    if(!dynamic_cast<Dolgozo*>(jelenlegi_felhasznalo.get())) { // Csak külsősök adhatnak meg kuponkódot
        cout << "Kupon: ";
        kuponkod = beker_string_ures();
    }
    Arajanlat arajanlat(szobatipusok[szoba_idx],
                        felnottek,
                        gyerekek,
                        kezdes,
                        vegzes,
                        ellatas);
    if(kuponkod.empty()) {
        ar=arajanlat.ajanlat();
    } else {
        arajanlat.setKupon(kuponkod);
        ar=arajanlat.ajanlat();
    }
    cout << "Árajánlat: " << ar  << " Ft" << endl;
    return arajanlat;
}

void Hotel::foglalas()
{
    Arajanlat aj = arajanlat_keszites();
    vector<Vendeg> vendegek;
    string email;
    bool foglalt;
    int szobaszam = 0;
    if(dynamic_cast<Dolgozo*>(jelenlegi_felhasznalo.get())){
        email = beker_string("A foglaló e-mail címe: ");
    } else {
        email = jelenlegi_felhasznalo->getLogin();
    }
    vector<int> szobalista = aj.getSzobatipus().szoba_lista();
    for(auto it = szobalista.begin(); it != szobalista.end(); ++it) {
        foglalt = false;
        for(auto& f: foglalasok) {
            if((*it) == f.getSzobaszam() && ( // szobaszam egyezik és átfedés van az időszakok között
                 aj.getKezdes().between(f.getKezdete(), f.getVege()) ||
                 aj.getVegzes().between(f.getKezdete(), f.getVege()))){
                foglalt = true;
                break;
            }
        } // Nincs átfedés vagy nincs a szobára foglalás -> mentjük
        if(!foglalt) {
            szobaszam = (*it);
            break;
        }
    }
    // Ha abszolút nincs megfelelő szoba, kilépünk
    if(szobaszam == 0) {
        cout << "Nincs üres szoba az adott időszakra!" << endl;
        return;
    }
    vector<int> szobak={szobaszam};
    for(int i=0; i < aj.getLetszam();i++){
        cout << i+1 << ". vendég adatai: " << endl;
        string nev = beker_string("Név: ");
        Datum szuldat = beker_datum("Születési dátum: ");
        Vendeg v(nev, szuldat);
        vendegek.push_back(v);
    }
    if(!beker_string("Véglegesíti a foglalást? (i/n): ").compare("i")) {
        Foglalas f(aj, vendegek, email, szobak);
        foglalasok.push_back(f);
    }
    return;
}

void Hotel::regisztracio()
{
    string nev = beker_string("Név: ");
    string email = beker_string("E-mail cím: ");
#ifdef __linux__
    string jelszo(getpass("Jelszó: "));
#else
    string jelszo(beker_string("Jelszó: "));
#endif
    RegisztraltVendeg rv(nev, email, jelszo);
    regisztralt_vendegek.push_back(rv);
}

bool Hotel::belepes()
{
    int opcio;
    string email, jelszo, azonosito;
    pair<string,string> azonositok;
    bool sikertelen = false;
    opcio = beker_lista({"Belépés regisztrációval",
                         "Belépés dolgozóként",
                         "Folytatás bejelentkezés nélkül"});
    if(opcio == 1) {
        email = beker_string("E-mail cím: ");
    }
    else if(opcio == 2) {
        azonosito = beker_string("Azonosító: ");
    }
    if(opcio != 3) {
#ifdef __linux__
        jelszo = getpass("Jelszó: ");
#else
        jelszo = beker_string("Jelszó: ");
#endif
    }
    if(!email.empty()) {
        auto it = find_if(regisztralt_vendegek.begin(),
                          regisztralt_vendegek.end(),
                          [&email](const RegisztraltVendeg& v){
            return !v.getLogin().compare(email);
        });
        if(it != regisztralt_vendegek.end()) {
            if((*it).azonositas({email,jelszo}))
                jelenlegi_felhasznalo = make_shared<RegisztraltVendeg>(*it);
            else
                sikertelen = true;
        } else {
            sikertelen = true;
        }
    }
    if(!azonosito.empty()) {
        auto it = find_if(dolgozok.begin(),
                          dolgozok.end(),
                          [&azonosito](const Dolgozo& v){
            return !v.getLogin().compare(azonosito);
        });
        if(it != dolgozok.end()) {
            if((*it).azonositas({azonosito,jelszo}))
                jelenlegi_felhasznalo = make_shared<Dolgozo>(*it);
            else
                sikertelen = true;
        } else {
            sikertelen = true;
        }
    }
    if(azonosito.empty() && email.empty()) {
        jelenlegi_felhasznalo = nullptr;
    }
    return sikertelen;
}

void Hotel::foglalasok_vendeg()
{
    int cnt = 0;
    for(auto &i: foglalasok) {
        if(!i.getLefoglalo().compare(jelenlegi_felhasznalo->getLogin())) {
            cnt++;
            cout << cnt << ". foglalás:" << endl;
            cout << i << endl;
            cout << "----------------------" << endl;
        }
    }
    if(cnt == 0) {
        cout << "Nem találtunk foglalálst!" << endl;
    }
}

void Hotel::foglalasok_recepcios()
{
    int cnt = 1;
 //   string nev = beker_string("Keresendő név: ");
    string email = beker_string("Kérem a foglalásnál használt e-mail címet: ");
/*    for(auto &i: regisztralt_vendegek) {
        if(!i.getNev().compare(nev)) {
            email=i.getLogin();
            break;
        }
    }
    if(email.empty()) {
        cout << "Nincs ilyen nevű vendég" << endl;
        return;
    }*/
    for(auto &i: foglalasok) {
        if(!i.getLefoglalo().compare(email)) {
            cout << cnt << ". foglalás:" << endl;
            cout << i << endl;
            cout << "----------------------" << endl;
            cnt++;
        }
    }
}

void Hotel::foglalas_torles()
{
    /*if(dynamic_cast<Dolgozo*>(jelenlegi_felhasznalo.get()))*/
    int cnt = 0;
    int torlendo = 0;
    string nev = beker_string("Keresendő név: ");
    string email;
    for(auto &i: regisztralt_vendegek) {
        if(!i.getNev().compare(nev)) {
            email=i.getLogin();
            break;
        }
    }
    if(email.empty()) {
        cout << "Nincs ilyen nevű vendég" << endl;
        return;
    }
    for(auto &i: foglalasok) {
        if(!i.getLefoglalo().compare(email)) {
            cnt++;
            cout << cnt << ". foglalás:" << endl;
            cout << i << endl;
            cout << "----------------------" << endl;
        }
    }
    torlendo = beker_int("Törlendő foglalás sorszáma: ");
    cnt = 1;
    for(auto it = foglalasok.begin();
        it != foglalasok.end();
        ++it) {
        if(!(*it).getLefoglalo().compare(email)) {
            if(torlendo == cnt) {
                foglalasok.erase(it);
                return;
            } else {
                cnt++;
            }
        }
    }
}

void Hotel::szoba_lekeres()
{
    bool volt_talalat = false;
    Szobatipus* szt = nullptr;
    int szobaszam = beker_int("Kérem adja meg a szobaszámot: ");
    int eredmeny;
    // Keressük meg a szobát először
    for(auto &sz: szobatipusok) {
        eredmeny = sz.szoba_kereses(szobaszam);
        if(eredmeny != -1) {
            szt = &sz;
        }
    }
    if(szt == nullptr) {
        cout << "Ez a szobaszám nem létezik" << endl;
        return;
    }

    // Keresés foglalások között
    for(auto& f: foglalasok) {
        eredmeny = f.szoba_kereses(szobaszam);
        if(eredmeny != -1) {
            volt_talalat = true;
            cout << f.getLefoglalo() << endl;
            cout << f << endl;
        }
    }
    if(!volt_talalat) {
        cout << "A szobához nem tartozik foglalás" << endl;
    }
}

void Hotel::ellatottak_szama()
{
    int cnt = 0;
    int etkezes = beker_lista({"Reggeli", "Ebéd", "Vacsora"})-1;
    vector<int> ellatas_lista;
    for(auto &f: foglalasok) {
        if(f.getAllapot() == h_enum::bejelentkezett) {
            int vendegek = f.getVendegek_szama();
            if (etkezes == 0) { // Reggeli
                if(f.getEllatas() != h_enum::nincs) {
                        cnt+=vendegek;
                }
            } else if (etkezes == 1) { // Ebéd
                if(f.getEllatas() == h_enum::teljes) {
                        cnt+=vendegek;
                }
            } else { // Vacsora
                if(f.getEllatas() == h_enum::teljes || f.getEllatas() == h_enum::felpanzio) {
                    cnt+= vendegek;
                }
            }
        }
    }

    cout << "Ellátottak száma: " << cnt << endl;
}

void Hotel::vendeg_ellatas_lekerese()
{
    int szobaszam = beker_int("Szobaszám: ");
    bool talalat = false;
    map<int,string> ellatas_megnevezesek{
        {h_enum::nincs, "Nincs"},
        {h_enum::reggeli, "Reggeli"},
        {h_enum::felpanzio, "Félpanzió"},
        {h_enum::teljes, "Teljes"}
    };

    for(auto& f: foglalasok) {
        if(f.getAllapot() == h_enum::bejelentkezett && f.getSzobaszam() == szobaszam) {
            cout << "Ellátás: " << ellatas_megnevezesek[f.getEllatas()] << endl;
            talalat = true;
        }
    }
    if(!talalat) {
        cout << "Nincs találat" << endl;
    }
}

void Hotel::bejelentkeztetes()
{
    const chrono::time_point<chrono::system_clock> now{chrono::system_clock::now()};
    const date::year_month_day ymd{date::floor<date::days>(now)};
    int cnt = 1;
    vector<Foglalas*> mai;
    // Ma kezdődő foglalások
    for(auto& f: foglalasok) {
        if(f.getKezdete().get_date() == ymd) {
            mai.push_back(&f);
            cout << cnt  << ". foglalás" << endl;
            cout << f << endl;
        }
    }
    if(mai.empty()) {
        cout << "Nincs mai nappal kezdődő foglalás" << endl;
        return;
    }
    int sorszam = beker_int("Foglalás sorszáma: ");
    mai[sorszam-1]->bejelentkezes();
    cout << "Bejelentkeztetés megtörtént" << endl;
}

void Hotel::kijelentkeztetes()
{
    const chrono::time_point<chrono::system_clock> now{chrono::system_clock::now()};
    const date::year_month_day ymd{date::floor<date::days>(now)};
    int cnt = 1;
    vector<Foglalas*> mai;
    // Ma véget érő foglalások
    for(auto& f: foglalasok) {
        if(f.getVege().get_date() == ymd) {
            mai.push_back(&f);
            cout << cnt  << ". foglalás" << endl;
            cout << f << endl;
        }
    }
    if(mai.empty()) {
        cout << "Nincs mai nappal végződő foglalás" << endl;
        return;
    }
    int sorszam = beker_int("Foglalás sorszáma: ");
    mai[sorszam-1]->kijelentkezes();
    cout << "Kijelentkeztetés megtörtént" << endl;
    string valasz = beker_string("A számla megjelenítése (i/n): ");
    if(!valasz.compare("i") || !valasz.compare("I")) {
        mai[sorszam-1]->getSzamla().kiir();
    }
}

void Hotel::fogyasztas_hozzaadas()
{
    int szobaszam = beker_int("Szobaszám: ");
    Foglalas *talalat = nullptr;
    for(auto& f: foglalasok) {
        if(f.getAllapot() == h_enum::bejelentkezett && f.getSzobaszam() == szobaszam) {
             talalat = &f;
        }
    }
    if(talalat == nullptr) {
        cout << "A szobához nem tartozik aktív foglalás" << endl;
        return;
    }
    string nev = beker_string("Tétel megnevezése:");
    int darab = beker_int("Darabszáma: ");
    int ar = beker_int("Ára: ");
    talalat->tetel_felszamolas(nev, darab, ar);
}

void Hotel::alkalmazott_torlese()
{
    string azonosito = beker_string("Törlendő alkalmazott azonosítója: ");
    for(auto it = dolgozok.begin(); it != dolgozok.end(); ++it) {
        if(!it->getLogin().compare(azonosito)) {
            dolgozok.erase(it);
            cout << "Alkalmazott törölve" << endl;
            return;
        }
    }
    cout << "Nincs ilyen azonosítójú alkalmazott" << endl;
}

void Hotel::alkalmazott_hozzadasa()
{
    string azonosito = beker_string("Azonosító: ");
    int munkakor = beker_lista({"Recepciós","Éttermi","Admin"});
#ifdef __linux__
    string jelszo(getpass("Jelszó: "));
#else
    string jelszo(beker_string("Jelszó: "));
#endif
    dolgozok.push_back(Dolgozo(azonosito,munkakor-1, jelszo));
    cout << "Alkalmazott hozzáadva" << endl;
}

void Hotel::szoba_modositas()
{
    vector<string> szoba_megnevezesek;
    int alapar;
    for(auto& sz: szobatipusok) {
        szoba_megnevezesek.push_back(sz.getMegnevezes());
    }
    int modositando = beker_lista(szoba_megnevezesek)-1;
    alapar = szobatipusok[modositando].getAlapar();
    int uj_alapar = beker_modositasra_int("Szoba jelenlegi alapára: ", alapar);
    if (uj_alapar != alapar) {
        szobatipusok[modositando].setAlapar(uj_alapar);
    }
    // TODO többi adat módosítása
}

void Hotel::foglalas_modositas()
{
    string email = beker_string("Kérem a foglalásnál használt e-mail címet: ");
    int cnt = 1;
    for(auto &i: foglalasok) {
        if(!i.getLefoglalo().compare(email)) {
            cout << cnt << ". foglalás:" << endl;
            cout << i << endl;
            cout << "----------------------" << endl;
            cnt++;
        }
    }
    int modositando = beker_int("Módosítani kívánt foglalás: ");
    cnt = 1;
    Foglalas* f = nullptr;
    for(auto &i: foglalasok) {
        if(!i.getLefoglalo().compare(email) && cnt == modositando) {
            f = &i;
        }
    }
    if(f) {
        cout << "Kért ellátás:" << endl;
        int uj_ellatas = beker_lista({"Nincs", "Reggeli", "Félpanzió", "Teljes"})-1;
        f->setEllatas(uj_ellatas);
        cout << "Módosítva" << endl;
    }
}

void Hotel::start()
{
    bool kilepes = false;
    vector<int> menu_list;
    vector<pair<int,string>> menu_lista_nevekkel;
    vector<string> nevlista;
    // Belépés
    while(belepes()){
        cout << "Hibás név vagy jelszó" << endl;
    }
    if(jelenlegi_felhasznalo) {
        menu_list = jelenlegi_felhasznalo->getMenulista();
    } else {
        menu_list = {m_regisztracio, m_arajanlat};
    }

    copy_if(menu_nevek.begin(),
            menu_nevek.end(),
            back_inserter(menu_lista_nevekkel),
            [&menu_list](const pair<int,string>& p){
        auto it =find_if(menu_list.begin(),
                         menu_list.end(),
                         [p](int i){
            return p.first == i;
        });
        return it != menu_list.end();
    });

    transform(menu_lista_nevekkel.begin(),
              menu_lista_nevekkel.end(),
              back_inserter(nevlista),
              [](const pair<int,string> p){
        return p.second;
    });
    while(!kilepes) {
        int valasztas = beker_lista(nevlista);

        int menupont = menu_lista_nevekkel[valasztas-1].first;

        switch (menupont) {
        case m_arajanlat:
            arajanlat_keszites();
            break;
        case m_foglalas:
            foglalas();
            break;
        case m_foglalas_lekeres_vendeg:
            foglalasok_vendeg();
            break;
        case m_foglalas_lekeres_recepcios:
            foglalasok_recepcios();
            break;
        case m_foglalas_torles:
            foglalas_torles();
            break;
        case m_kilepes:
            kilepes = true;
            break;
        case m_szoba_allapot_lekeres:
            szoba_lekeres();
            break;
        case m_ellatotak_szama:
            ellatottak_szama();
            break;
        case m_ellatas_lekeres:
            vendeg_ellatas_lekerese();
            break;
        case m_bejelentkeztetes:
            bejelentkeztetes();
            break;
        case m_kijelentkeztetes:
            kijelentkeztetes();
            break;
        case m_fogyasztas_hozzaadas:
            fogyasztas_hozzaadas();
            break;
        case m_alkalmazott_torles:
            alkalmazott_torlese();
            break;
        case m_alkalmazott_hozzaadas:
            alkalmazott_hozzadasa();
            break;
        case m_szoba_modositas:
            szoba_modositas();
            break;
        case m_foglalas_modositas:
            foglalas_modositas();
            break;
        default:
            cout << "Nincs implementálva" << endl;
        }
        if(!kilepes) {
            getchar();
        }
    }


    // Teszt kiírások
//    cout << beker_string("Név: ") << endl;
//    cout << beker_int("Felnőttek: ") << endl;
//    for_each(foglalasok.begin(),
//             foglalasok.end(),
//             [](const Foglalas& f){
//       f.megjelenit();
//    });
//    for_each(regisztralt_vendegek.begin(),
//             regisztralt_vendegek.end(),
//             [](const RegisztraltVendeg& f){
//       f.megjelenit();
//    });

    // Foglalas teszt
//    Foglalas f=foglalas();
//    foglalasok.push_back(f);

    // Regisztracio teszt
//    regisztracio();

    ment();
}
